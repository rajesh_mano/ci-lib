// vars/getPomVersion.groovy
def call(String file = 'pom.xml') {
    def matcher = readFile(file) =~ '<version>(.+)</version>'
    return matcher ? matcher[0][1] : null
}