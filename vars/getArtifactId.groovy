// vars/getArtifactId.groovy
def call(String path = 'pom.xml') {
    def matcher = readFile(file) =~ '<artifactId>(.+)</artifactId>'
    return matcher ? matcher[0][1] : null
}