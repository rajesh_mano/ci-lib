// vars/sayHello.groovy
def call(String name = 'human') {
    // Any valid steps can be called from this code, just like in other
    // Scripted Pipeline
    echo "Hello, ${name}."

    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
   return matcher ? matcher[0][1] : null

}