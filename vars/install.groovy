// vars/install.groovy
def call(String path = 'pom.xml', String args = null) {
    if(args!=null){
        bat 'mvn clean deploy -P' + args + ' -f'+ path
    } else {
        bat 'mvn clean deploy' + ' -f'+ path
    }
}